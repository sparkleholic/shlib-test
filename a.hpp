#ifndef __PERSON__
#define __PERSON__

#include <iostream>

class Person
{
public:
    void setName(std::string n);
    std::string getName();

private:
    int age;
    std::string name;
};

extern "C" {
    int getInitNum();
}

#endif