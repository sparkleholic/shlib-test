#ifndef __UNIVERSITY__
#define __UNIVERSITY__

#include <iostream>
#include "a.hpp"

class University {
public:
    // friend std::string getTeacherName(University& u);
    std::string getTeacherName();
    University(std::string t, std::string s) {
        teacher.setName(t);
        student.setName(s);
    }

    Person teacher;
    Person student;
};

// std::string getTeacherName(University& u);
#endif