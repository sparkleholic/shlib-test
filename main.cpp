#include <iostream>
#include "b.hpp"

int main(int argc, char** argv) {
    std::cout << "Hello developer!" << std::endl;
    University u("Names", "Kock");
    // std::cout << "Teacher Name:" << getTeacherName(u) << std::endl;
    std::cout << "Teacher Name:" << u.getTeacherName() << std::endl;
    return 0;
}